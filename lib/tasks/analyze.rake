# frozen_string_literal: true

# Don't crash in production
begin
  require 'bundler/audit/task'
  require 'rubocop/rake_task'

  Bundler::Audit::Task.new
  RuboCop::RakeTask.new
rescue LoadError
  puts 'Audit task and/or Rubocop failed to load'
end
